# A Shiny tool
Interactive dashboard for visualising and identifying KPI trends across markets.

## Setup

Run the following in the project directory:

```
install.packages("packrat")
packrat::restore()

# Check status to see everything is OK
packrat::status()

# Start the application
shiny::runApp()

```
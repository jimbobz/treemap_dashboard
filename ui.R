header <- dashboardHeader(title = "NOODLES!")

sidebar <- dashboardSidebar(disable=TRUE)

body <- dashboardBody(
  fluidRow(
    # LEFT: Plots
    column(width=9,
      fluidRow(
        box(
          title="Treemap",
          width=12, collapsible=TRUE,
          highchartOutput("treemap", height="550px")
        )
      ),
      fluidRow(
        box(
          title="Trendline",
          width=12, collapsible=TRUE,
          highchartOutput("trendline", height="300px")
        )
      )
    ),
    # RIGHT: Controls
    column(width=3,
      fluidRow(
        box(
          title="Controls", status="warning",
          width=12, collapsible=TRUE,
          fluidRow(
            column(
              width=6,
              uiOutput("app_ui"),
              uiOutput("kpi_ui")
            ),
            column(
              width=6,
              uiOutput("from_date_ui"),
              uiOutput("dimension_ui")
            )
          ),
          fluidRow(
            column(
              width=8,
              uiOutput("target_date_ui"),
              uiOutput("reference_date_ui")
            ),
            column(
              width=4,
              uiOutput("tier_ui")
            )
          ),
          fluidRow(
            column(width=6, radioButtons("grouping", "Tier grouping", choices=c("On","Off"), selected="Off", inline=TRUE)),
            column(width=6, numericInput("range_cutoff", "Threshold (%)", value=10, min=1, max=30, step=1, width="50%"))
          ),
          fluidRow(
            column(
              width=12,
              actionButton("update_button", "Update"),
              actionButton("reset_button", "", icon=icon("refresh")),
              br(), br(), htmlOutput("data_freshness")
            )
          )
        )
      ),
      # DataTable
      fluidRow(
        box(
          width=12, title="Highlights",
          dataTableOutput("table")
        )
      )
    )
  )
)

dashboardPage(skin="purple", header, sidebar, body)

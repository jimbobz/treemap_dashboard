
# Slices data for treemap and calculates difference between target and reference periods
treemapSlice <- function(data, kpi, target, reference, tiers) {
  df <- data %>%
    filter(((dt >= target[1] & dt <= target[2]) | (dt >= reference[1] & dt <= reference[2])), tier %in% tiers) %>%
    mutate(is_reference = (dt >= reference[1] & dt <= reference[2])) %>%
    group_by(tier, country, country_name, is_reference) %>%
    summarise_("kpi_avg"=paste0("mean(",kpi,",na.rm=TRUE)")) %>%
    group_by(country) %>% arrange(desc(is_reference)) %>%
    mutate(change=100*(kpi_avg-lag(kpi_avg))/lag(kpi_avg)) %>%
    filter(!is_reference) %>%
    ungroup()

  return(df)
}


# Filters data per country for trendline plot
trendlineSlice <- function(data, category, kpi) {
  df_slice <- data %>% filter(country %in% category) %>%
    mutate_("kpi" = kpi) %>%
    arrange(dt)

  return(df_slice)
}

# Ranks table for table output
rankTable <- function(data) {
  tbl <- data %>% 
    filter(is.finite(change)) %>% 
    arrange(desc(abs(kpi_avg*change))) %>% 
    transmute(
      "CC" = country,
      "Name"=country_name,
      "Tier" = tier,
      "KPI" = round(kpi_avg),
      "Change" = paste(round(change,1),"%")
    )
  return(tbl)
}


# Creates treemap object and chart
plotTreemap <- function(data, tmIndex, kpi_col, cutoff=10) {
  tryCatch({
    # Treemap object
    tm <- treemap(
      data, index=tmIndex, vSize="kpi_avg", vColor="change",
      type="value", draw=FALSE, palette="RdYlGn", mapping=c(-cutoff,0,cutoff), range=c(-100,100)
    )
    kpi_name <- dimensions[['kpi']][dimensions[['kpi']]$kpi==kpi_col, "name"]
  
    # Chart
    chart <- hctreemap(tm, allowDrillToNode=TRUE, layoutAlgorithm="squarified") %>%
      hc_title(text = kpi_name) %>%
      hc_subtitle(text="Click to toggle trendline") %>%
      hc_tooltip(
        pointFormat = paste0("<b>{point.name}</b>:<br>",kpi_name,": {point.value}<br>Change: {point.valuecolor:,.1f} %"),
        valueDecimals = 0
      ) %>%
      hc_plotOptions(
        series=list(events=list(
          click=JS("function(event) {
            Shiny.onInputChange('category-click', event.point.name);
          }")
        )))
  
    return(chart)
  }, error = function(e) {
    print(e$message)
    return(highchart())
  })
  
}


# Plots trendline with highlighted areas
plotTrendline <- function(data, kpi, category, target, reference) {
  tryCatch({
    # Calculate date differences
    target_diff <- as.numeric(target[2]-target[1])
    reference_diff <- as.numeric(reference[2]-reference[1])
    target <- datetime_to_timestamp(as.Date(target))
    reference <- datetime_to_timestamp(as.Date(reference))
    df_plot <- data %>% 
      mutate(label = paste0(country_name, " (", country, ")"))
    
    # Create chart
    chart <- hchart(df_plot, "line", hcaes(x=dt, y=kpi, group=label)) %>%
      hc_xAxis(
        type="datetime", title=list(text=""),
        dateTimeLabelFormats=list(day="%Y-%m-%d", week="%Y-%m-%d", month="%Y-%m-%d"),
        labels=list(rotation=-90, y=12, x=4),
        plotBands = list(
          list(
            from=target[1], to=target[2], color="rgba(120, 180, 120, 0.1)", 
            label = list(text = paste0("<font color='grey'>T", " (",target_diff,"d)</font>"), useHTML=TRUE, align="center")
          ),
          list(
            from=reference[1], to=reference[2], color="rgba(180, 180, 180, 0.1)",
            label = list(text = paste0("<font color='grey'>R", " (",reference_diff,"d)</font>"), useHTML=TRUE, align="center")
          )
        )
      ) %>%
      hc_yAxis(title=list(text=dimensions[['kpi']][dimensions[['kpi']]$kpi==kpi, "unit"]), softMin=0) %>%
      hc_legend(
        layout="vertical",
        align="right",
        verticalAlign="middle"
      ) %>%
      hc_tooltip(
        shared=TRUE,
        valueDecimals=0
      ) %>%
      hc_chart(zoomType="xy")
    
    return(chart)
  }, error = function(e) {
    print(e$message)
    return(highchart())
  })
}
